# SpingCodeGenFX介绍

本工具用于从 ***满足下划线命名风格*** 的sql建表脚本生成全套增删改查Restful接口，且包括注释和swagger注解。

## 安装说明

在任意非中文目录下解压SpringCodeGenFX-${version}.zip文件

找到根目录SpringCodeGenFX下的SpringCodeGenFX.exe 右键创建快捷方式，复制快捷方式到适当位置。

## 使用说明
### 1)
双击快捷方式打开SpringCodeGenFX.exe。
#### 从建表语句生成
![DDL生成](https://img-blog.csdnimg.cn/20200521104504616.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2p1bmlvcl9wcm9ncmFtbWVy,size_16,color_FFFFFF,t_70)

#### 表已经在数据库中，可直接从表名生成
![表名生成](https://img-blog.csdnimg.cn/20200521104957767.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2p1bmlvcl9wcm9ncmFtbWVy,size_16,color_FFFFFF,t_70)
### 2)
为了避免提交插件生成的代码文件夹**需要在项目的.gitignore中 添加 \**/sqlgen**，忽略生成代码的文件夹。

### 3)
***自动生成的代码少不了冗余和错漏***，所以没有直接生成到项目的src/main/java路径下。

在当前的目录结构下，只要将src/main/sqlgen/java加入Module的Sources中即可直接启动项目进行接口功能测试。


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200521105638949.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2p1bmlvcl9wcm9ncmFtbWVy,size_16,color_FFFFFF,t_70)

测试修改完善代码后转移到按项目规范所属的package。

PS：可使用快捷键Ctrl+Alt+L对生成的代码进行格式自动调整，可使用快捷键Ctrl+Alt+O对多余的import进行自动清理，
或者通过文件夹右键选择reformat code进行批量格式化。 

### 4)
***升级***

低于2.0.1版本需要删除整个安装文件夹

2.0.1以上版本升级只需替换掉SpringCodeGenFX\app\sqlgen文件夹
及SpringCodeGenFX\app\SpringCodeGenFX-jfx.jar

### 5)

***自定义模板***(如无特殊需求，不建议修改模板)

将安装目录SpringCodeGenFX\app\sqlgen文件夹复制到当前设置的工作目录下，按需修改sqlgen\config\templates\win目录下的ftl文件，
测试稳定后直接替换调安装目录SpringCodeGenFX\app\sqlgen文件夹即可
