package com.ysstech.codegen.constant;

public class GlobalConst {
    public final static String PLUGIN_WORK_DIR = "sqlgen";

    public final static String CONFIG_RELATIVE_DIR = "config";
    public final static String CONFIG_DIR = PLUGIN_WORK_DIR+"/"+CONFIG_RELATIVE_DIR;
    public final static String CONFIG_FILE_NAME = "param.json";
    public final static String CONFIG_FILE_PATH = PLUGIN_WORK_DIR+"/"+CONFIG_RELATIVE_DIR+"/"+CONFIG_FILE_NAME;

    public final static String TEMPLATE_RELATIVE_DIR = "templates";
    public final static String TEMPLATE_BASE_DIR = PLUGIN_WORK_DIR+"/"+CONFIG_RELATIVE_DIR;
    public final static String TEMPLATE_DIR = PLUGIN_WORK_DIR+"/"+CONFIG_RELATIVE_DIR+"/"+TEMPLATE_RELATIVE_DIR;

    public final static String CODE_RELATIVE_DIR = "java";
    public final static String CODE_FILE_PATH = PLUGIN_WORK_DIR+"/"+ CODE_RELATIVE_DIR;
}
