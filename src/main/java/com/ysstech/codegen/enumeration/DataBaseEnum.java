/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2020/1/19/19:03
 * 项目名称: SpringCodeGenFX
 * 文件名称: DataBaseDriverNameEnum.java
 * 文件描述: 数据库连接驱动枚举类
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.ysstech.codegen.enumeration;

/**
 * 包名称：com.ysstech.codegen.enumeration
 * 类名称：DataBaseDriverNameEnum
 * 类描述：数据库连接驱动枚举类
 * 创建人：@author fengxin
 * 创建时间：2020/1/19/19:03
 */
public enum DataBaseEnum {
    MYSQL("mysql", "com.mysql.cj.jdbc.Driver"),
    //Oracle("oracle", "oracle.jdbc.driver.OracleDriver"),

    ;
    private String name;
    private String driver;

    DataBaseEnum(String name, String driver) {
        this.name = name;
        this.driver = driver;
    }

    public static String getDriverByName(String name) {
        String driver = "";
        for (DataBaseEnum dataBaseEnum : DataBaseEnum.values()) {
            if (dataBaseEnum.name.equals(name)) {
                driver = dataBaseEnum.driver;
            }
        }
        return driver;
    }

    public String getName() {
        return name;
    }

    public String getDriver() {
        return driver;
    }
}
