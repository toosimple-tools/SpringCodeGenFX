package com.ysstech.codegen.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FileExtEnum {
    SQL(".sql","SQL script"),
    JAVA(".java","java code"),
    XML(".xml","xml file"),
    TXT(".txt","txt file"),
    ;
    String ext;
    String desc;

    public static FileExtEnum getFileExtEnum(String ext) {
        for (FileExtEnum fileExtEnum: FileExtEnum.values()
        ) {
            if (fileExtEnum.getExt().equals(ext)) {
                return fileExtEnum;
            }
        }
        return TXT;
    }

}
