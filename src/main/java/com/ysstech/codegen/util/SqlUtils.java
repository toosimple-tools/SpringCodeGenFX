package com.ysstech.codegen.util;

import kotlin.text.MatchResult;
import kotlin.text.Regex;
import kotlin.text.RegexOption;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SqlUtils {
    public static boolean isNotCreateTable(String sql) {
        Set<RegexOption> options = new HashSet<>();
        options.add(RegexOption.IGNORE_CASE);
        Regex regex = new Regex("CREATE[\\s]+TABLE[\\s]+[\\s\\S]*?",options);
        MatchResult matchResult = regex.find(sql,0);
        return matchResult == null;
    }

    public static boolean isUniqueKey(String columnLine,List<String> uniqueKeys) {
        columnLine=columnLine.replaceAll("['`]","");
        String pattern = "((((unique)|(primary))\\s+)|(^\\s*?))key((\\s+)|(\\s+\\S+\\s+))\\(\\s*?(\\S+)\\s*?\\)";
        Set<RegexOption> options = new HashSet<>();
        options.add(RegexOption.IGNORE_CASE);
        options.add(RegexOption.MULTILINE);
        Regex regex = new Regex(pattern,options);
        MatchResult matchResult = regex.find(columnLine,0);
        if (matchResult != null) {
            // TODO 特殊字体的，和 英文, 不一样 识别错误 暂用注释下方替代方案
//            uniqueKeys.addAll( Arrays.asList(matchResult.getGroupValues()
//                    .get(matchResult.getGroupValues().size()-1).split("[,，]")));
            uniqueKeys.addAll( Arrays.asList(matchResult.getGroupValues()
                    .get(matchResult.getGroupValues().size()-1).split("[^0-9a-zA-Z_]")));
        }
        return matchResult!=null;
    }

}
