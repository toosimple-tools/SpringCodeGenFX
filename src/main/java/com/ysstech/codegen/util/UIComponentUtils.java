/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2020/1/19/21:13
 * 项目名称: SpringCodeGenFX
 * 文件名称: UIComponentUtils.java
 * 文件描述: UI组件工具
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.ysstech.codegen.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import oracle.jrockit.jfr.jdkevents.ThrowableTracer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;

/**
 * 包名称：com.ysstech.codegen.util
 * 类名称：UIComponentUtils
 * 类描述：UI组件工具
 * 创建人：@author fengxin
 * 创建时间：2020/1/19/21:13
 */
public class UIComponentUtils {


    public static void createAlertDialog(AlertType alertType, final String content) {
        Alert alert = new Alert(alertType);
        alert.setHeight(600);
        alert.setWidth(800);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }


    public static void createAlertDialog(AlertType alertType, final String content, final Throwable throwable) {
        Alert alert = new Alert(alertType);
        alert.setHeight(600);
        alert.setWidth(800);
        alert.setHeaderText(null);
        alert.setContentText(content);

        if (Objects.nonNull(throwable)) {
            // Create expandable Exception.
            GridPane expContent = createExpandablePane(throwable);
            // Set expandable Exception into the dialog pane.
            alert.getDialogPane().setExpandableContent(expContent);
            alert.getDialogPane().setExpanded(true);
        }
        alert.showAndWait();
    }

    private static GridPane createExpandablePane(final Throwable throwable) {
        String exceptionText = "";

        if (Objects.nonNull(throwable)) {
            StringWriter sWriter = new StringWriter();
            PrintWriter pWriter = new PrintWriter(sWriter);
            throwable.printStackTrace(pWriter);
            exceptionText = sWriter.toString();
        }

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);
        return expContent;
    }
}
