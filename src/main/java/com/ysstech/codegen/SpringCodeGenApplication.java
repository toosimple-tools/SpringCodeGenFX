package com.ysstech.codegen;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 * 包名称：com.ysstech.codegen
 * 类名称：SpringCodeGenService
 * 类描述：生成spring代码的service
 * 创建人：@author fengxin
 * 创建时间：2019/12/21/15:32
 */
public class SpringCodeGenApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("SpringCodeGen.fxml"));
        primaryStage.setTitle("SpringCodeGen");
        primaryStage.setScene(new Scene(root, 800, 790));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(SpringCodeGenApplication.class.getResourceAsStream("/SpringCodeGen.png")));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
