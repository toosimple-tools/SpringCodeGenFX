package com.ysstech.codegen.service;

import com.ysstech.codegen.entity.LabelInfo;
import com.ysstech.codegen.entity.ParamInfo;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public interface SpringCodeGenService {

    void initWorkDirPath(String appWorkDirPath);
    void loadConfig();
    void genCode(String fileText);
    Set<String> getSelectedCodeLabels();
    void addSelectedCodeLabel(String label);
    void removeSelectedCodeLabel(String label);
    ParamInfo getParamInfo();
    String getAppWorkDirPath();
    Map<String, LabelInfo> getLabelInfoMap();
    Map<String, String> getLabelToTemplateNameMap();
    Map<String, String>  getLabelToSubPackageMap();
    Map<String, String> getLabelToFileExtMap();
    String getResultByParamsAndLabel(Map<String, Object> params, String label) throws IOException, TemplateException;

}
