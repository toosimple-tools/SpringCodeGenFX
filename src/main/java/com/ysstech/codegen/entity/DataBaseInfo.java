/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2020/1/19/18:14
 * 项目名称: SpringCodeGenFX
 * 文件名称: DataBase.java
 * 文件描述: 数据库连接信息
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.ysstech.codegen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 包名称：com.ysstech.codegen.entity
 * 类名称：DataBase
 * 类描述：数据库连接信息
 * 创建人：@author fengxin
 * 创建时间：2020/1/19/18:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataBaseInfo {
    String dataBase = "mysql";
    String host = "192.168.0.107";
    int port = 3307;
    String schemaName = "devdb";
    String user = "root";
    String password = "123456";
}
