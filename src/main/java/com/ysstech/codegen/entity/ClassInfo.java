package com.ysstech.codegen.entity;

import lombok.Data;

import java.util.List;

/**
 * class info
 *
 * @author xuxueli 2018-05-02 20:02:34
 */
@Data
public class ClassInfo {

    private String tableName;
    private String className;
	private String classComment;
	private List<FieldInfo> fieldList;
	private List<FieldInfo> uniqueKeyList;
	private List<FieldInfo> baseEntityFieldList;
	private List<FieldInfo> auditFieldList;
	private List<FieldInfo> subEntityFieldList;
	private List<FieldInfo> subRepFieldList;
	private List<FieldInfo> subReqFieldList;

}