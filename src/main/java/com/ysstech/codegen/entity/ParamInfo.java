package com.ysstech.codegen.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * Post data - ParamInfo
 * @author zhengkai.blog.csdn.net
 */
@Data
public class ParamInfo {
    private String tableSql;
    private String authorName;
    @JSONField(name="appPackageName")
    private String packageName;
    private String moduleName;
    @JSONField(name="controllerResponseType")
    private String returnUtil;

    private String nameCaseType;
    private String tinyintTransType;
    private String datetimeTransType;
    private String dataType;
    private List<String> baseEntityFields;
    private List<String> auditFields;
    private List<String> baseRepFields;
    private List<String> baseReqFields;

    @Data
    public static class NAME_CASE_TYPE{
        public static String CAMEL_CASE="CamelCase";
        public static String UNDER_SCORE_CASE="UnderScoreCase";
        public static String UPPER_UNDER_SCORE_CASE="UpperUnderScoreCase";
    }
}
