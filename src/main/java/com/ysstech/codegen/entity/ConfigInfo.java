/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/9/11:42
 * 项目名称: SpringCodeGen
 * 文件名称: ConfigInfo.java
 * 文件描述: @Description: 配置信息
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.ysstech.codegen.entity;

import lombok.Data;

import java.util.Map;

/**
 * 包名称：com.free.plugin.codegen.entity
 * 类名称：ConfigInfo
 * 类描述：配置信息
 * 创建人：@author fengxin
 * 创建时间：2019/12/9/11:42
 */
@Data
public class ConfigInfo {
    ParamInfo paramInfo;
    Map<String,LabelInfo> labelInfoMap;
}
