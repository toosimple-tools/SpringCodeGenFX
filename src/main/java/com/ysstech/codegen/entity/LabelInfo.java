package com.ysstech.codegen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LabelInfo {
    String targetSubPackage;
    String targetFileExt;
    String templateName;
}
