package com.ysstech.codegen.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ServerResponse<T> implements Serializable {
    public static final long serialVersionUID = 42L;
    public static final int SUCCESS_CODE = 200;
    public static final int FAIL_CODE = 500;
    public static final ServerResponse<String> SUCCESS = new ServerResponse<>(null);
    public static final ServerResponse<String> FAIL = new ServerResponse<>(FAIL_CODE, null);
    private int code;
    private String msg;
    private T data;

    public ServerResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ServerResponse(T data) {
        this.code = SUCCESS_CODE;
        this.data = data;
    }
}