/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/12/21/20:40
 * 项目名称: SpringCodeGenFX
 * 文件名称: UserOperationInfo.java
 * 文件描述: 用户操作信息，用于保存用户最近的选项和工作目录
 *
 * All rights Reserved, Designed By 投资交易团队
 * @Copyright:2016-2019
 *
 ********************************************************/
package com.ysstech.codegen.entity;

import lombok.Data;

import java.util.Set;

/**
 * 包名称：com.ysstech.codegen.entity
 * 类名称：UserOperationInfo
 * 类描述：用户操作信息，用于保存用户最近的选项和工作目录
 * 创建人：@author fengxin
 * 创建时间：2019/12/21/20:40
 */

@Data
public class UserOperationInfo {
    Set<String> selectedCodeLabels;
    String appWorkDirPath;
    String basePackageName;
    String moduleName;
    DataBaseInfo dataBaseInfo = new DataBaseInfo();
}
