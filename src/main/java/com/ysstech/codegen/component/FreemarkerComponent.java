package com.ysstech.codegen.component;

import com.ysstech.codegen.constant.GlobalConst;
import com.ysstech.codegen.exception.CodeGenerateException;
import com.ysstech.codegen.util.StringUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Getter
@Setter
public class FreemarkerComponent {
    String fullTemplateDir;

    public FreemarkerComponent(String fullTemplateDir) {
        this.fullTemplateDir = fullTemplateDir;
        Configuration freemarkerConfig = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        try {
            freemarkerConfig.setDirectoryForTemplateLoading(new File(fullTemplateDir));
            freemarkerConfig.setNumberFormat("#");
            freemarkerConfig.setClassicCompatible(true);
            freemarkerConfig.setDefaultEncoding("UTF-8");
            freemarkerConfig.setLocale(Locale.CHINA);
            freemarkerConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            configuration = freemarkerConfig;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public FreemarkerComponent(ClassLoader classLoader, String templateClasspathDir) {
        this.fullTemplateDir = classLoader.getResource(templateClasspathDir).getPath();
        Configuration freemarkerConfig = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        freemarkerConfig.setClassLoaderForTemplateLoading(classLoader, templateClasspathDir);
        freemarkerConfig.setNumberFormat("#");
        freemarkerConfig.setClassicCompatible(true);
        freemarkerConfig.setDefaultEncoding("UTF-8");
        freemarkerConfig.setLocale(Locale.CHINA);
        freemarkerConfig.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        configuration = freemarkerConfig;
    }

    private Configuration configuration;

    /**
     * process Template Into String
     *
     * @param template
     * @param model
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    public String processTemplateIntoString(Template template, Object model)
            throws IOException, TemplateException {

        StringWriter result = new StringWriter();
        template.process(model, result);
        return result.toString();
    }

    /**
     * process String
     *
     * @param templateName
     * @param params
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    public String processString(String templateName, Map<String, Object> params)
            throws IOException, TemplateException {
        String htmlText = null;
        try {
            Template template = configuration.getTemplate(templateName);
            htmlText = processTemplateIntoString(template, params);
        } catch (Exception e) {
            throw new CodeGenerateException("获取模板失败,请确认模板:【"
                    + fullTemplateDir + "/" + templateName
                    + "】存在,错误信息:" + e.getMessage(), e);
        }
        return htmlText;
    }

}