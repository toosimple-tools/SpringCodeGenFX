<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"QryReqVO" classComment=classInfo.classComment+"QryReqVO"/>

package ${packageName}.${subPackageName};

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.win.dfas.common.util.LongJsonDeserializer;
import com.win.dfas.common.util.LongJsonSerializer;
import com.win.dfas.common.vo.BaseQryReqVO;
import com.win.dfas.common.validation.ValidationGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"QryReqVO" classComment=classInfo.classComment+"QryReqVO"/>

<@commonJavaClass classDesc=classInfo.classComment+"QryReqVO" className=classInfo.className+"QryReqVO" classCategory="ReqVO" fieldList=classInfo.subReqFieldList/>