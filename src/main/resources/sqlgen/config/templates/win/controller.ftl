<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"Controller" classComment=classInfo.classComment+"Controller"/>

package ${packageName}.controller;

import com.win.dfas.common.vo.${returnUtil};
import ${packageName}.service.${classInfo.className}Service;
import ${packageName}.vo.request.${classInfo.className}AddReqVO;
import ${packageName}.vo.request.${classInfo.className}UpdateReqVO;
import ${packageName}.vo.request.${classInfo.className}QryReqVO;
import ${packageName}.vo.response.${classInfo.className}RepVO;
import com.win.dfas.common.validation.ValidationGroup;

import com.github.pagehelper.PageInfo;
import com.alibaba.fastjson.JSON;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"Controller" classComment=classInfo.classComment+"Controller"/>


@Api(tags="${classInfo.classComment}")
@RestController
@RequestMapping(value = "/${classInfo.className?uncap_first}")
public class ${classInfo.className}Controller {

    @Autowired
    private ${classInfo.className}Service ${classInfo.className?uncap_first}Service;

    <@commonSwaggerPlacehold operValue="新增" paramType=classInfo.className+"AddReqVO" />
    @PostMapping("/save")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="save" methodDesc="新增" returnType=returnUtil paramType=classInfo.className+"AddReqVO"/>


    <@commonSwaggerPlacehold operValue="更新" paramType=classInfo.className+"UpdateReqVO" />
    @PutMapping("/updateById")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="updateById" methodDesc="更新" returnType=returnUtil paramType=classInfo.className+"UpdateReqVO"/>


    <@commonSwaggerPlacehold operValue="逻辑删除" paramType="Long" />
    @DeleteMapping("/deleteById/{id}")
    WinResponseData<Void> deleteById(@PathVariable("id") Long id) {
        <#--LOGGER.info("/deleteById/deleteById, ${classInfo.classComment}数据逻辑删除请求入参: {}", id));-->
        ${classInfo.className?uncap_first+"Service"}.deleteById(id);
        return WinResponseData.handleSuccess("删除数据成功");
    }

    <@commonSwaggerPlacehold operValue="列表查询" paramType=classInfo.className+"ReqVO" />
    @PostMapping("/list")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="List<"+classInfo.className+"RepVO>"
    methodName="list" methodDesc="列表查询" returnType=returnUtil paramType=classInfo.className+"QryReqVO"/>


    <@commonSwaggerPlacehold operValue="分页查询" paramType=classInfo.className+"ReqVO" />
    @PostMapping("/pageList")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="PageInfo<"+classInfo.className+"RepVO>"
    methodName="pageList" methodDesc="分页查询" returnType=returnUtil paramType=classInfo.className+"QryReqVO"/>


    <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
    <#list classInfo.fieldList as fieldItem >
    <#if fieldItem.fieldName == "auditStatus">
    <@commonSwaggerPlacehold operValue="审核"  paramType="Long" />
    @PutMapping("/updateAudit/{id}")
    WinResponseData<Void> updateAuditById(@PathVariable("id") Long id) {
        ${classInfo.className?uncap_first+"Service"}.updateAuditById(id);
        return WinResponseData.handleSuccess("审核数据成功");
    }

    <@commonSwaggerPlacehold operValue="反审核" paramType="Long" />
    @PutMapping("/updateRevertAudit/{id}")
    WinResponseData<Void> updateRevertAuditById(@PathVariable("id") Long id) {
        ${classInfo.className?uncap_first+"Service"}.updateRevertAuditById(id);
        return WinResponseData.handleSuccess("反审核数据成功");
    }
	</#if>
	</#list>
	</#if>
}
