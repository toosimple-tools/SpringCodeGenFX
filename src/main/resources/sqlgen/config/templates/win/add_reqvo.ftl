<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"AddReqVO" classComment=classInfo.classComment+"AddReqVO"/>

package ${packageName}.${subPackageName};

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.win.dfas.common.validation.ValidationGroup;
import com.win.dfas.common.vo.BaseAddReqVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"AddReqVO" classComment=classInfo.classComment+"AddReqVO"/>

<@commonJavaClass classDesc=classInfo.classComment+"AddReqVO" className=classInfo.className+"AddReqVO" classCategory="ReqVO" fieldList=classInfo.subEntityFieldList/>