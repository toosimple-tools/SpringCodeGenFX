<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader  className=classInfo.className+"ServiceImpl" classComment=classInfo.classComment+"ServiceImpl" />

package ${packageName}.${subPackageName};

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.win.dfas.common.util.ObjectUtils;
import com.win.dfas.common.util.UserUtil;
import com.win.dfas.common.util.WinExceptionUtil;
import com.win.dfas.common.exception.WinException;
import com.win.dfas.common.enumeration.StatusEnum.AuditStatusEnum;
import ${packageName}.entity.${classInfo.className};
import ${packageName}.service.${classInfo.className}Service;
import ${packageName}.dao.${classInfo.className}Mapper;
import ${packageName}.vo.request.${classInfo.className}AddReqVO;
import ${packageName}.vo.request.${classInfo.className}UpdateReqVO;
import ${packageName}.vo.request.${classInfo.className}QryReqVO;
import ${packageName}.vo.response.${classInfo.className}RepVO;

import java.util.List;
import java.util.Objects;

<@commonJavaClassCommnet subPackageName=subPackageName  className=classInfo.className+"ServiceImpl" classComment=classInfo.classComment+"ServiceImpl" />

@Service
@Transactional(rollbackFor = Exception.class)
public class ${classInfo.className}ServiceImpl implements ${classInfo.className}Service {

	@Autowired
	private ${classInfo.className}Mapper ${classInfo.className?uncap_first}Mapper;

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper" methodName="save" returnType="int" paramType=classInfo.className+"AddReqVO"/>


	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper" methodName="updateById" returnType="int" paramType=classInfo.className+"UpdateReqVO"/>


	@Override
	public int deleteById(Long id) {
		${classInfo.className} ${classInfo.className?uncap_first} = ${classInfo.className?uncap_first}Mapper.getById(id);
		if(ObjectUtil.isNull(${classInfo.className?uncap_first})) {
			throw new WinException("id不存在");
		}
		${classInfo.className?uncap_first}.preDelete();
		return ${classInfo.className?uncap_first}Mapper.updateById(${classInfo.className?uncap_first});
	}

	@Override
	public ${classInfo.className}RepVO getById(Long id) {
		${classInfo.className} ${classInfo.className?uncap_first} = ${classInfo.className?uncap_first}Mapper.getById(id);
		if(ObjectUtil.isNull(${classInfo.className?uncap_first})) {
			return null;
		}
		${classInfo.className}RepVO ${classInfo.className?uncap_first}RepVO =  new ${classInfo.className}RepVO();
		BeanUtils.copyProperties(${classInfo.className?uncap_first},${classInfo.className?uncap_first}RepVO);
		return ${classInfo.className?uncap_first}RepVO;
	}

	@Override
	public List<${classInfo.className}RepVO> list(${classInfo.className}QryReqVO reqVO) {
		List<${classInfo.className}> list = ${classInfo.className?uncap_first}Mapper.list(reqVO);
		return ObjectUtils.copyPropertiesList(list, ${classInfo.className}RepVO.class,true);
	}

	@Override
	public PageInfo<${classInfo.className}RepVO> pageList(${classInfo.className}QryReqVO reqVO) {
		PageHelper.startPage(reqVO.getReqPageNum(), reqVO.getReqPageSize());
		List<${classInfo.className}> list = ${classInfo.className?uncap_first}Mapper.list(reqVO);
		PageInfo<${classInfo.className}> pageInfo = new PageInfo<>(list);
		return ObjectUtils.copyPageInfo(pageInfo, ${classInfo.className}RepVO.class,true);
	}
	<#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0><#list classInfo.fieldList as fieldItem ><#if fieldItem.fieldName == "auditStatus">	@Override
	public int updateAuditById(Long id) {
		${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
		${classInfo.className?uncap_first}.setId(id);
		${classInfo.className?uncap_first}.setAuditStatus(AuditStatusEnum.AUDITED.getStatusCode());
		${classInfo.className?uncap_first}.setAuditTime(DateUtil.date());
		${classInfo.className?uncap_first}.setAuditUserId(UserUtil.getUserId());
		return ${classInfo.className?uncap_first}Mapper.updateById(${classInfo.className?uncap_first});
	}
	@Override
	public int updateRevertAuditById(Long id) {
		${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
		${classInfo.className?uncap_first}.setId(id);
		${classInfo.className?uncap_first}.setAuditStatus(AuditStatusEnum.NON_AUDITED.getStatusCode());
		// TODO 反审核时可能需要检查数据是否正在其他关联表中使用
        ${classInfo.className?uncap_first}.setAuditTime(DateUtil.date());
        ${classInfo.className?uncap_first}.setAuditUserId(UserUtil.getUserId());
		return ${classInfo.className?uncap_first}Mapper.updateById(${classInfo.className?uncap_first});
	}
		<#break>
	</#if>
	</#list>
	</#if>

}
