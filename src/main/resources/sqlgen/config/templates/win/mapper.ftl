<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"Mapper" classComment=classInfo.classComment+"Mapper" />

package ${packageName}.${subPackageName};

import ${packageName}.entity.${classInfo.className};
import ${packageName}.vo.request.${classInfo.className}QryReqVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

<@commonJavaClassCommnet subPackageName=subPackageName  className=classInfo.className+"Mapper" classComment=classInfo.classComment+"Mapper"/>

@Mapper
public interface ${classInfo.className}Mapper {

    <@commonJavaMethodAndCommnet methodDesc="新增" methodName="save" returnType="int" paramType=classInfo.className/>;

    <@commonJavaMethodAndCommnet methodDesc="修改" methodName="updateById" returnType="int" paramType=classInfo.className/>;

    <@commonJavaMethodAndCommnet methodDesc="删除" methodName="deleteById" returnType="int" paramType="Long" paramName="id"/>;

    <@commonJavaMethodAndCommnet methodDesc="查询" methodName="getById" returnType=classInfo.className paramType="Long" paramName="id"/>;

    <@commonJavaMethodAndCommnet methodDesc="列表查询" methodName="list" returnType="List<"+classInfo.className+">" paramType=classInfo.className+"QryReqVO"/>;

}
