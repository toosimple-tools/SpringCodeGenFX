<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${packageName}.dao.${classInfo.className}Mapper">

    <resultMap id="BaseResultMap" type="${packageName}.entity.${classInfo.className}" >
        <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
            <#list classInfo.fieldList as fieldItem >
                <result column="${fieldItem.columnName}" property="${fieldItem.fieldName}" />
            </#list>
        </#if>
    </resultMap>

    <sql id="Base_Column_List">
        <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
            <#list classInfo.fieldList as fieldItem >
                ${fieldItem.columnName}<#if fieldItem_has_next>,</#if>
            </#list>
        </#if>
    </sql>

    <insert id="save" parameterType="${packageName}.entity.${classInfo.className}">
        INSERT INTO ${classInfo.tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
                <#list classInfo.fieldList as fieldItem >
                    <#--<#if fieldItem.columnName != "id" >-->
                        ${r"<if test ='null != "}${fieldItem.fieldName}${r"'>"}
                        ${fieldItem.columnName}<#if fieldItem_has_next>,</#if>
                        ${r"</if>"}
                    <#--</#if>-->
                </#list>
            </#if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
                <#list classInfo.fieldList as fieldItem >
                    <#--<#if fieldItem.columnName != "id" >-->
                    <#--<#if fieldItem.columnName="addtime" || fieldItem.columnName="updatetime" >
                    ${r"<if test ='null != "}${fieldItem.fieldName}${r"'>"}
                        NOW()<#if fieldItem_has_next>,</#if>
                    ${r"</if>"}
                    <#else>-->
                        ${r"<if test ='null != "}${fieldItem.fieldName}${r"'>"}
                        ${r"#{"}${fieldItem.fieldName}${r"}"}<#if fieldItem_has_next>,</#if>
                        ${r"</if>"}
                    <#--</#if>-->
                    <#--</#if>-->
                </#list>
            </#if>
        </trim>
    </insert>

    <delete id="deleteById">
        DELETE FROM ${classInfo.tableName}
        <where>
          ${r"and id = #{id}"}
            <#list classInfo.uniqueKeyList as fieldItem >
                <#if fieldItem.columnName != "id" >
          ${r"<!-- <if test ='null != "}${fieldItem.fieldName}${r" and null == id'> -->"}
          <!-- and ${fieldItem.columnName} = ${r"#{"}${fieldItem.fieldName}${r"}"} -->
          ${r"<!-- </if> -->"}
                </#if>
            </#list>
        </where>
    </delete>

    <update id="updateById">
        UPDATE ${classInfo.tableName}
        <set>
        <#list classInfo.fieldList as fieldItem >
        <#if fieldItem.columnName != "id" >
            ${r"<if test ='null != "}${fieldItem.fieldName}${r"'>"}
            ${fieldItem.columnName} = ${r"#{"}${fieldItem.fieldName}${r"}"}<#if fieldItem_has_next>,</#if>
            ${r"</if>"}
        </#if>
        </#list>
        </set>
        <where>
            and ${r"id = #{id}"}
            <#list classInfo.uniqueKeyList as fieldItem >
            <#if fieldItem.columnName != "id" >
            ${r"<!-- <if test ='null != "}${fieldItem.fieldName}${r" and null == id'> -->"}
            <!-- and ${fieldItem.columnName} = ${r"#{"}${fieldItem.fieldName}${r"}"} -->
            ${r"<!-- </if> -->"}
            </#if>
            </#list>
        </where>
    </update>

    <select id="getById" resultMap="BaseResultMap" >
        SELECT <include refid="Base_Column_List" />
        FROM ${classInfo.tableName}
        <where>
            and ${r"id = #{id}"}
            <#list classInfo.uniqueKeyList as fieldItem >
            <#if fieldItem.columnName != "id" >
            ${r"<!-- <if test ='null != "}${fieldItem.fieldName}${r" and null == id'> -->"}
            <!-- and ${fieldItem.columnName} = ${r"#{"}${fieldItem.fieldName}${r"}"} -->
            ${r"<!-- </if> -->"}
            </#if>
            </#list>
        </where>
    </select>

    <select id="list" resultMap="BaseResultMap" parameterType="${packageName}.vo.request.${classInfo.className}QryReqVO">
        SELECT <include refid="Base_Column_List" />
        FROM ${classInfo.tableName}
        <where>
            and delete_flag = 0
        <#list classInfo.subReqFieldList as fieldItem >
            <#if fieldItem.columnName != "id" && fieldItem.columnName != "delete_flag"  >
            ${r"<if test ='null != "}${fieldItem.fieldName}${r"'>"}
            and ${fieldItem.columnName} = ${r"#{"}${fieldItem.fieldName}${r"}"}
            ${r"</if>"}
            </#if>
        </#list>
        </where>
    </select>


</mapper>