<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"Service" classComment=classInfo.classComment+"Service" />

package ${packageName}.${subPackageName};

import com.github.pagehelper.PageInfo;
import ${packageName}.entity.${classInfo.className};
import ${packageName}.vo.request.${classInfo.className}AddReqVO;
import ${packageName}.vo.request.${classInfo.className}UpdateReqVO;
import ${packageName}.vo.request.${classInfo.className}QryReqVO;


import ${packageName}.vo.response.${classInfo.className}RepVO;

import java.util.List;

<@commonJavaClassCommnet subPackageName=subPackageName  className=classInfo.className+"Service" classComment=classInfo.classComment+"Service"/>


public interface ${classInfo.className}Service {

    <@commonJavaMethodAndCommnet methodDesc="新增" methodName="save" returnType="int" paramType=classInfo.className+"AddReqVO" paramName="reqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="修改" methodName="updateById" returnType="int" paramType=classInfo.className+"UpdateReqVO" paramName="reqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="逻辑删除" methodName="deleteById" returnType="int" paramType="Long" paramName="id"/>;

    <@commonJavaMethodAndCommnet methodDesc="查询" methodName="getById" returnType=classInfo.className+"RepVO" paramType="Long" paramName="id"/>;

    <@commonJavaMethodAndCommnet methodDesc="列表查询" methodName="list" returnType="List<"+classInfo.className+"RepVO>" paramType=classInfo.className+"QryReqVO" paramName="reqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="分页查询" methodName="pageList" returnType="PageInfo<"+classInfo.className+"RepVO>" paramType=classInfo.className+"QryReqVO" paramName="reqVO"/>;

    <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
    <#list classInfo.fieldList as fieldItem >
    <#if fieldItem.fieldName == "auditStatus">
    <@commonJavaMethodAndCommnet methodDesc="更新审核状态为已审核" methodName="updateAuditById" returnType="int" paramType="Long" paramName="id"/>;
    <@commonJavaMethodAndCommnet methodDesc="更新审核状态为未审核" methodName="updateRevertAuditById" returnType="int" paramType="Long" paramName="id"/>;
	</#if>
	</#list>
	</#if>
}
