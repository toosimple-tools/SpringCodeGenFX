<#macro commonJavaFileHeader className classComment>/****************************************************
* 创建人：@author ${authorName}
* 创建时间: ${.now?string('yyyy-MM-dd')}
* 项目名称: ${moduleName}
* 文件名称: ${className}.java
* 文件描述: ${classComment}
*
* All Rights Reserved, Designed By 投资交易团队
* @Copyright:${.now?string('yyyy')}-${(.now?string('yyyy')?number+10)}
*
********************************************************/
</#macro>

<#macro commonJavaClassCommnet subPackageName className classComment>/**
* 包名称：${packageName}.${subPackageName}
* 类名称：${className}
* 类描述：${classComment}
* 创建人：@author ${authorName}
* 创建时间：${.now?string('yyyy-MM-dd')}
*/
</#macro>

<#--校验注解-->
<#macro controllerValidate methodName>@Validated({ValidationGroup.<#if methodName=="save">Add<#elseif methodName=="updateById">Update<#elseif methodName=="pageList">Query<#else >PageQuery</#if>.class})</#macro>
<#macro reqVOValidate className>{<#if className?contains("AddReqVO")>ValidationGroup.Add.class<#elseif className?contains("UpdateReqVO")>ValidationGroup.Update.class<#elseif className?contains("QryReqVO")>ValidationGroup.PageQuery.class<#else >ValidationGroup.Default.class</#if>}</#macro>



<#macro commonJavaClass classDesc className classCategory fieldList>
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
    <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
        <#list classInfo.fieldList as fieldItem >
            <#if fieldItem.fieldName == "auditStatus" && classCategory=="Entity">
public class ${className} extends BaseAuditableEntity {
                <#break>
            <#elseif fieldItem.fieldName == "auditStatus" && classCategory=="RepVO" >
public class ${className} extends BaseAuditableRepVO {
                <#break>
            <#else>
                <#if !fieldItem_has_next>
public class ${className} extends <#if className?contains("QryReqVO")>BaseQryReqVO<#elseif className?contains("AddReqVO")>BaseAddReqVO<#elseif className?contains("UpdateReqVO")>BaseUpdateReqVO<#else>Base${classCategory}</#if> {
                </#if>
            </#if>
        </#list>
    </#if>

    <#if classCategory=="Entity" || classCategory=="RepVO" || classCategory=="ReqVO" || classCategory=="DTO">
    private static final long serialVersionUID = 666L;
    </#if>

    <#if className?contains("QryReqVO") || className?contains("UpdateReqVO")>
        <#if className?contains("UpdateReqVO")>
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @ApiModelProperty(value = "数据id",required = true)
    @NotNull(message = "数据ID不能为空")
            <#else>
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @ApiModelProperty(value = "数据id")
        </#if>
    private Long id;
    </#if>

    <#if className?contains("QryReqVO")>
    <#if fieldList?exists && fieldList?size gt 0>
        <#list fieldList as fieldItem >
    @ApiModelProperty(value = "${fieldItem.fieldComment}")
    private ${fieldItem.fieldClass} ${fieldItem.fieldName};

        </#list>
    </#if>
    <#else>
        <#if fieldList?exists && fieldList?size gt 0>
            <#list fieldList as fieldItem >
                <#if !fieldItem.fieldName?contains("audit")>
                    <#if classCategory=="ReqVO" >
                        <#if classInfo.uniqueKeyList?seq_contains(fieldItem)>
    @ApiModelProperty(value = "${fieldItem.fieldComment}",required = true)
    <#if fieldItem.fieldClass =="String">@NotBlank<#else>@NotNull</#if>(message = "${fieldItem.fieldComment}不能为空",groups=<@reqVOValidate className=className/>)
    private ${fieldItem.fieldClass} ${fieldItem.fieldName};

                        <#else>
    @ApiModelProperty(value = "${fieldItem.fieldComment}")
    private ${fieldItem.fieldClass} ${fieldItem.fieldName};

                        </#if>
                    <#else >
    @ApiModelProperty(value="${fieldItem.fieldComment}")
    private ${fieldItem.fieldClass} ${fieldItem.fieldName};

                    </#if>
                </#if>
            </#list>
        </#if>
    </#if>


}
</#macro>


<#macro commonJavaMethodAndCommnet methodDesc methodName returnType paramType paramName="">/**
    * ${methodDesc}
    * @param <#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>
    * @return ${returnType}
    * @throws
    * @author ${authorName}
    * @date ${.now?string('yyyy-MM-dd')}
    */
    ${returnType} ${methodName}(${paramType} <#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>)</#macro>


<#macro commonJavaServiceImplMethodAndCommnet mapperType methodName returnType paramType paramName="reqVO">
    @Override
    public ${returnType} ${methodName}(${paramType} <#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>){
    <#if methodName=="save"  || methodName=="updateById" >
        <#if methodName=="save" >
        // TODO 需要补充业务唯一键的存在性校验
        </#if>
        <#--新增修改时 参数类型需要转换-->
        ${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
        BeanUtils.copyProperties(<#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>, ${classInfo.className?uncap_first});
        <#if methodName=="updateById" >
        ${classInfo.className?uncap_first}.preUpdate();
        <#else>
        ${classInfo.className?uncap_first}.pre${methodName?cap_first}();
        </#if>
        return ${mapperType?uncap_first}.${methodName}(${classInfo.className?uncap_first});
    <#else >
        return ${mapperType?uncap_first}.${methodName}(<#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>);
    </#if>
    }</#macro>


<#macro commonJavaControllerMethodAndCommnet serviceType serviceReturnType  methodName methodDesc returnType paramType paramName="reqVO">
    public ${returnType}<#if serviceReturnType=="int"><${"Void"}><#else><${serviceReturnType}></#if> ${methodName}(@RequestBody <@controllerValidate methodName=methodName/> ${paramType} ${paramName}){
        <#--LOGGER.info("/${classInfo.className?uncap_first}/${methodName}, ${classInfo.classComment}数据${methodDesc}请求入参: {}", JSON.toJSONString(${paramName}));-->
    <#if serviceReturnType=="int">
        ${serviceType?uncap_first}.${methodName}(<#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>);
        return ${returnType}.handleSuccess("${methodDesc}数据成功");
    <#else>
        return ${returnType}.handleSuccess(${serviceType?uncap_first}.${methodName}(<#if paramName=="">${paramType?uncap_first}<#else>${paramName}</#if>));
    </#if>
    }</#macro>



