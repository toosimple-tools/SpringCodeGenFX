<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"Service" classComment=classInfo.classComment+"Service" />

package ${packageName}.${subPackageName};

import com.github.pagehelper.PageInfo;
import ${packageName}.entity.${classInfo.className};
import ${packageName}.vo.request.${classInfo.className}ReqVO;
import ${packageName}.vo.response.${classInfo.className}RepVO;

import java.util.List;

<@commonJavaClassCommnet subPackageName=subPackageName  className=classInfo.className+"Service" classComment=classInfo.classComment+"Service"/>


public interface ${classInfo.className}Service {

    <@commonJavaMethodAndCommnet methodDesc="新增" methodName="save" returnType="int" paramType=classInfo.className+"ReqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="修改" methodName="update" returnType="int" paramType=classInfo.className+"ReqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="逻辑删除" methodName="delete" returnType="int" paramType=classInfo.className+"ReqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="查询" methodName="get" returnType=classInfo.className paramType=classInfo.className+"ReqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="列表查询" methodName="list" returnType="List<"+classInfo.className+">" paramType=classInfo.className+"ReqVO"/>;

    <@commonJavaMethodAndCommnet methodDesc="分页查询" methodName="pageList" returnType="PageInfo<"+classInfo.className+"RepVO>" paramType=classInfo.className+"ReqVO"/>;

    <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
    <#list classInfo.fieldList as fieldItem >
    <#if fieldItem.fieldName == "auditStatus">
    <@commonJavaMethodAndCommnet methodDesc="更新审核状态为已审核" methodName="updateAudit" returnType="int" paramType=classInfo.className+"ReqVO"/>;
    <@commonJavaMethodAndCommnet methodDesc="更新审核状态为未审核" methodName="updateRevertAudit" returnType="int" paramType=classInfo.className+"ReqVO"/>;
	</#if>
	</#list>
	</#if>
}
