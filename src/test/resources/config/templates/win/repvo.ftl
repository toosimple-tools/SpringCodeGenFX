<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader  className=classInfo.className+"RepVO" classComment=classInfo.classComment+"RepVO"/>

package ${packageName}.${subPackageName};

import com.win.dfas.common.vo.BaseRepVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"RepVO" classComment=classInfo.classComment+"RepVO"/>

<@commonJavaClass classDesc=classInfo.classComment+"RepVO" className=classInfo.className+"RepVO" classCategory="RepVO" fieldList=classInfo.subRepFieldList/>