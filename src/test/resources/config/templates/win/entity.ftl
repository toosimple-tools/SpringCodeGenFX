<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className classComment=classInfo.classComment/>

package ${packageName}.${subPackageName};

import com.win.dfas.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;


<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className classComment=classInfo.classComment/>

<@commonJavaClass classDesc=classInfo.classComment className=classInfo.className classCategory="Entity" fieldList=classInfo.subEntityFieldList/>