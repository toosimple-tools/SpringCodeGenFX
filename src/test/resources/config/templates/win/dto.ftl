<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"DTO" classComment=classInfo.classComment+"DTO" />

package ${packageName}.${subPackageName};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"DTO" classComment=classInfo.classComment+"DTO"/>

<@commonJavaClass classDesc=classInfo.classComment+"DTO" className=classInfo.className+"DTO" classCategory="DTO" fieldList=classInfo.fieldList/>