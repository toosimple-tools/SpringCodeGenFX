<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader  className=classInfo.className+"ServiceImpl" classComment=classInfo.classComment+"ServiceImpl" />

package ${packageName}.${subPackageName};

import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.win.dfas.common.util.ObjectUtils;
import com.win.dfas.common.util.UserUtil;
import com.win.dfas.common.exception.WinException;
import com.win.dfas.common.enumeration.StatusEnum.AuditStatusEnum;
import ${packageName}.entity.${classInfo.className};
import ${packageName}.service.${classInfo.className}Service;
import ${packageName}.dao.${classInfo.className}Mapper;
import ${packageName}.vo.request.${classInfo.className}ReqVO;
import ${packageName}.vo.response.${classInfo.className}RepVO;

import java.util.List;
import java.util.Objects;

<@commonJavaClassCommnet subPackageName=subPackageName  className=classInfo.className+"ServiceImpl" classComment=classInfo.classComment+"ServiceImpl" />

@Service
@Transactional(rollbackFor = Exception.class)
public class ${classInfo.className}ServiceImpl implements ${classInfo.className}Service {

	@Autowired
	private ${classInfo.className}Mapper ${classInfo.className?uncap_first}Mapper;

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper" methodName="save" returnType="int" paramType=classInfo.className+"ReqVO"/>

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper" methodName="update" returnType="int" paramType=classInfo.className+"ReqVO"/>

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper"  methodName="delete" returnType="int" paramType=classInfo.className+"ReqVO"/>

	public boolean checkDelete(${classInfo.className} ${classInfo.className?uncap_first}) {
		// TODO 默认允许直接删除${classInfo.className?uncap_first}对应数据 请按照实际需求增加判断逻辑
		return true;
	}

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper"  methodName="get" returnType=classInfo.className paramType=classInfo.className+"ReqVO"/>

	<@commonJavaServiceImplMethodAndCommnet mapperType=classInfo.className+"Mapper"  methodName="list" returnType="List<"+classInfo.className+">" paramType=classInfo.className+"ReqVO"/>

	@Override
	public PageInfo<${classInfo.className}RepVO> pageList(${classInfo.className}ReqVO reqVO) {
		// 默认只查询delete_flag为0的记录
		reqVO.setDeleteFlag(0L);
		PageHelper.startPage(reqVO.getReqPageNum(), reqVO.getReqPageSize());
		List<${classInfo.className}> list = ${classInfo.className?uncap_first}Mapper.list(reqVO);
		PageInfo<${classInfo.className}> pageInfo = new PageInfo<>(list);
		return ObjectUtils.copyPageInfo(pageInfo, ${classInfo.className}RepVO.class);
	}

	<#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
	<#list classInfo.fieldList as fieldItem >
	<#if fieldItem.fieldName == "auditStatus">
	@Override
	public int updateAudit(${classInfo.className}ReqVO ${classInfo.className?uncap_first}ReqVO ) {
		// TODO 这里进行参数字段校验 或者在controller中通过注解实现
		${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
		${classInfo.className?uncap_first}.setId(${classInfo.className?uncap_first}ReqVO.getId());
		${classInfo.className?uncap_first}.setAuditStatus(AuditStatusEnum.AUDITED.getStatusCode());
		${classInfo.className?uncap_first}.setAuditTime(DateUtil.date().toString());
		${classInfo.className?uncap_first}.setAuditUserId(UserUtil.getUserId());
		return ${classInfo.className?uncap_first}Mapper.update(${classInfo.className?uncap_first});
	}
	@Override
	public int updateRevertAudit(${classInfo.className}ReqVO ${classInfo.className?uncap_first}ReqVO ) {
		// TODO 这里进行参数字段校验 或者在controller中通过注解实现
		${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
		${classInfo.className?uncap_first}.setId(${classInfo.className?uncap_first}ReqVO.getId());
		${classInfo.className?uncap_first}.setAuditStatus(AuditStatusEnum.NON_AUDITED.getStatusCode());
		// TODO 反审核时可能也需要检查数据是否正在其他关联表中使用
		${classInfo.className?uncap_first}.setAuditTime(DateUtil.date().toString());
		${classInfo.className?uncap_first}.setAuditUserId(UserUtil.getUserId());
		return ${classInfo.className?uncap_first}Mapper.update(${classInfo.className?uncap_first});
	}
	</#if>
	</#list>
	</#if>
}
