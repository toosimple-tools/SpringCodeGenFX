<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"ReqVO" classComment=classInfo.classComment+"ReqVO"/>

package ${packageName}.${subPackageName};

import com.win.dfas.common.vo.BaseReqVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"ReqVO" classComment=classInfo.classComment+"ReqVO"/>

<@commonJavaClass classDesc=classInfo.classComment+"ReqVO" className=classInfo.className+"ReqVO" classCategory="ReqVO" fieldList=classInfo.subReqFieldList/>