<#--此文件仅有声明作用 为了编译器能够进行代码提示-->
<#macro commonJavaFileHeader className classComment>
</#macro>

<#macro commonJavaClassCommnet subPackageName className classComment>
</#macro>

<#macro commonJavaMethodAndCommnet methodDesc methodName returnType paramType>
</#macro>

<#macro commonJavaServiceImplMethodAndCommnet mapperType methodName returnType paramType>
</#macro>

<#macro commonJavaControllerMethodAndCommnet serviceType serviceReturnType methodName methodDesc returnType paramType>
</#macro>

<#macro commonJavaClass classDesc className classCategory fieldList>
</#macro>