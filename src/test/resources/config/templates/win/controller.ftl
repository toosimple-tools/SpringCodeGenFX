<#include 'common/java.ftl'>
<#include 'common/swagger.ftl'>
<@commonJavaFileHeader className=classInfo.className+"Controller" classComment=classInfo.classComment+"Controller"/>

package ${packageName}.controller;


import com.win.dfas.common.vo.${returnUtil};
import ${packageName}.entity.${classInfo.className};
import ${packageName}.service.${classInfo.className}Service;
import ${packageName}.vo.request.${classInfo.className}ReqVO;
import ${packageName}.vo.response.${classInfo.className}RepVO;

import com.github.pagehelper.PageInfo;
import com.alibaba.fastjson.JSON;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

<@commonJavaClassCommnet subPackageName=subPackageName className=classInfo.className+"Controller" classComment=classInfo.classComment+"Controller"/>


@Api(tags="${classInfo.classComment}")
@RestController
@RequestMapping(value = "/${classInfo.className?uncap_first}")
public class ${classInfo.className}Controller {
    private static final Logger LOGGER = LoggerFactory.getLogger(${classInfo.className}Controller.class);

    @Autowired
    private ${classInfo.className}Service ${classInfo.className?uncap_first}Service;

    <@commonSwaggerPlacehold operValue="新增" paramType=classInfo.className+"ReqVO" />
    @PostMapping("/save")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="save" methodDesc="新增" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>

    <@commonSwaggerPlacehold operValue="更新" paramType=classInfo.className+"ReqVO" />
    @PutMapping("/update")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="update" methodDesc="更新" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>

    <@commonSwaggerPlacehold operValue="逻辑删除" paramType=classInfo.className+"ReqVO" />
    @DeleteMapping("/delete")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="delete" methodDesc="删除" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>

    <@commonSwaggerPlacehold operValue="查询" paramType=classInfo.className+"ReqVO" />
    @PostMapping("/get")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType=classInfo.className
    methodName="get" methodDesc="查询" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>

    <@commonSwaggerPlacehold operValue="分页查询" paramType=classInfo.className+"ReqVO" />
    @PostMapping("/pageList")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="PageInfo<"+classInfo.className+"RepVO>"
    methodName="pageList" methodDesc="分页查询" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>

    <#if classInfo.fieldList?exists && classInfo.fieldList?size gt 0>
    <#list classInfo.fieldList as fieldItem >
    <#if fieldItem.fieldName == "auditStatus">
    <@commonSwaggerPlacehold operValue="审核" paramType=classInfo.className+"ReqVO" />
    @PutMapping("/updateAudit")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="updateAudit"  methodDesc="审核" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>
    <@commonSwaggerPlacehold operValue="反审核" paramType=classInfo.className+"ReqVO" />
    @PutMapping("/updateRevertAudit")
    <@commonJavaControllerMethodAndCommnet serviceType=classInfo.className+"Service" serviceReturnType="int"
    methodName="updateRevertAudit"  methodDesc="反审核" returnType=returnUtil paramType=classInfo.className+"ReqVO"/>
	</#if>
	</#list>
	</#if>

}
