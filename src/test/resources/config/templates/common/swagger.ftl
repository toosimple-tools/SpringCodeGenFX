<#macro commonSwaggerApiOperation >
    @ApiOperation(value = "", notes = "")
</#macro>

<#macro commonSwaggerApiParam >
    @ApiParam(name = "", value = "", required = false)
</#macro>

<#macro commonSwaggerPlacehold operValue paramType >
    @ApiOperation(value = "${operValue}")
</#macro>

<#macro commonSwaggerApiImplicitParams fieldList>
    @ApiImplicitParams({
    <#if fieldList?exists && fieldList?size gt 0>
        <#list fieldList as fieldItem >
        @ApiImplicitParam(name = "${fieldItem.fieldName}", value = "${fieldItem.fieldComment}", required = true, dataType = "${fieldItem.fieldClass}")<#if fieldItem_has_next>,</#if>
        </#list>
    </#if>})
</#macro>