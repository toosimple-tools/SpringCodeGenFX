<#macro commonJavaFileHeader className classComment>/****************************************************
* 创建人：@author ${authorName}
* 创建时间: ${.now?string('yyyy-MM-dd')}
* 项目名称: ${moduleName}
* 文件名称: ${className}.java
* 文件描述: ${classComment}
*
* All Rights Reserved, Designed By 投资交易团队
* @Copyright:${.now?string('yyyy')}-2030
*
********************************************************/
</#macro>

<#macro commonJavaClassCommnet subPackageName className classComment>/**
* 包名称：${packageName}.${subPackageName}
* 类名称：${className}
* 类描述：${classComment}
* 创建人：@author ${authorName}
* 创建时间：${.now?string('yyyy-MM-dd')}
*/
</#macro>

<#macro commonJavaMethodAndCommnet methodDesc methodName returnType paramType>/**
    * ${methodDesc}
    <#if methodName=="get" || methodName=="update" || methodName=="delete">
    </#if>
    * @param ${paramType?uncap_first}
    * @return ${returnType}
    * @throws
    * @author ${authorName}
    * @date ${.now?string('yyyy-MM-dd')}
    */
    ${returnType} ${methodName}(${paramType} ${paramType?uncap_first})</#macro>


<#macro commonJavaServiceImplMethodAndCommnet mapperType methodName returnType paramType>
    @Override
    public ${returnType} ${methodName}(${paramType} ${paramType?uncap_first}){

    <#if methodName=="save"  || methodName=="update" || methodName=="delete">
        <#if methodName!="delete">
        // TODO 这里进行参数字段校验 或者在controller中通过注解实现
        <#--增改删时 参数类型需要转换-->
        ${classInfo.className} ${classInfo.className?uncap_first} = new ${classInfo.className}();
        BeanUtils.copyProperties(${paramType?uncap_first}, ${classInfo.className?uncap_first});
        ${classInfo.className?uncap_first}.pre${methodName?cap_first}();
        return ${mapperType?uncap_first}.${methodName}(${classInfo.className?uncap_first});
        <#else>
        // 先查询明细数据
        ${classInfo.className} ${classInfo.className?uncap_first} = get(${classInfo.className?uncap_first}ReqVO);
        //BeanUtils.copyProperties(${classInfo.className?uncap_first}ReqVO, ${classInfo.className?uncap_first});

        // 检查是否可删除
        if(check${methodName?cap_first}(${classInfo.className?uncap_first})) {
            ${classInfo.className?uncap_first}.preDelete();
            return ${mapperType?uncap_first}.update(${classInfo.className?uncap_first});
        }else{
            // 不允许删除 抛出异常
            // TODO 按需求修改成通过WinExceptionUtil.winException基于对应的ExceptionEnum抛出异常
            throw new WinException("非法操作，数据正在使用不允许删除");
        }
        </#if>
    <#else>
        return ${mapperType?uncap_first}.${methodName}(${paramType?uncap_first});
    </#if>
    }</#macro>

<#macro commonJavaControllerMethodAndCommnet serviceType serviceReturnType  methodName methodDesc returnType paramType>
    public ${returnType}<#if serviceReturnType=="int"><${"Void"}><#else><${serviceReturnType}></#if> ${methodName}(
        @ApiParam(name = "${paramType?uncap_first}", value = "${methodDesc}请求参数", required = true) @RequestBody ${paramType} ${paramType?uncap_first}){
        LOGGER.info("/${classInfo.className?uncap_first}/${methodName}, ${classInfo.classComment}数据${methodDesc}请求入参: {}", JSON.toJSONString(${paramType?uncap_first}));
    <#if serviceReturnType=="int">
        ${serviceType?uncap_first}.${methodName}(${paramType?uncap_first});
        return ${returnType}.handleSuccess("${methodDesc}数据成功");
    <#else>
        return ${returnType}.handleSuccess(${serviceType?uncap_first}.${methodName}(${paramType?uncap_first}));
    </#if>
    }</#macro>

<#macro commonJavaClass classDesc className classCategory fieldList>
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("${className}")
public class ${className} <#if classCategory=="DTO">implements Serializable<#else>extends Base${classCategory}</#if> {

<#if classCategory=="Entity" || classCategory=="RepVO" || classCategory=="ReqVO" || classCategory=="DTO">
    private static final long serialVersionUID = 666L;
</#if>

<#if fieldList?exists && fieldList?size gt 0>
    <#list fieldList as fieldItem >
    @ApiModelProperty("${fieldItem.fieldComment}")
    private ${fieldItem.fieldClass} ${fieldItem.fieldName};

    </#list>
</#if>
}
</#macro>