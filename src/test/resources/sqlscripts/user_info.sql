-- noinspection SqlDialectInspectionForFile
-- noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE 'user_info' (
  'id' bigint(22) NOT NULL  COMMENT '数据记录ID',
  'user_id' int(11) NOT NULL  COMMENT '用户ID',
  'user_name' varchar(255) NOT NULL COMMENT '用户名',
  'audit_user_id' varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '审核人',
  'audit_status' char(1) COLLATE utf8mb4_bin DEFAULT '1' COMMENT '审核状态',
  'audit_time' datetime DEFAULT NULL COMMENT '审核时间',
  'create_time' datetime NOT NULL COMMENT '创建时间',
  'delete_flag' bigint(19) NOT NULL DEFAULT '0' COMMENT '逻辑删除标志: 0-未删除、1-已删除',

  PRIMARY KEY ('id'),
  UNIQUE KEY 'uk_user_id' ('user_id'),
  KEY  'inx_user_name' ('user_name')
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin  COMMENT='用户信息';



